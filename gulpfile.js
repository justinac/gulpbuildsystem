var gulp        = require('gulp'),
    uglify      = require('gulp-uglify'),
    sass        = require('gulp-sass'),
    pug         = require('gulp-pug'),
    imagemin    = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

gulp.task('compile_sass', function() { 
  return gulp.src('src/sass/**/*.sass', { style: 'compressed' }) 
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('compress_images', function() {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/imgs/'))
});

gulp.task('server_start', function(){
    browserSync.init({
        server: {
            baseDir: './build/'
        }
    });

    gulp.watch('src/js/**/*.js', ['minify_js']);
    gulp.watch('src/sass/**/*.sass', ['compile_sass']);
    gulp.watch('src/sass/**/*.scss', ['compile_sass']);
    gulp.watch('src/pug/**/*.pug', ['compile_pug']);
    gulp.watch('build/*.html').on('change', browserSync.reload);
});

gulp.task('compile_pug', function() {
    gulp.src('src/pug/**/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('build/'));
});

gulp.task('minify_js', function() {
    gulp.src('src/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('build/js/'));
});

gulp.task('default', ['minify_js', 'compile_sass', 'server_start']);